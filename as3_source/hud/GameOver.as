package hud
{
	import com.funtotype.utils.FunLib;
	import com.greensock.TweenMax;
	import com.greensock.easing.Cubic;
	
	import flash.display.MovieClip;
	import flash.display.SimpleButton;
	import flash.events.Event;
	import flash.text.TextField;
	
	public class GameOver extends MovieClip
	{
		public var timePlayed:TextField;
		public var fruitsSliced:TextField;
		public var totalScore:TextField;
		
		public var mainMenu:SimpleButton;
		public var playAgain:SimpleButton;
		
		public function GameOver(startTime:Date, fruits:Number = 0, score:Number = 0)
		{
			x = 458;
			y = 420;
			
			var endTime:Date = new Date();
			var milliSec:Number = endTime.time - startTime.time;
			var totalSec:Number = Math.round(milliSec/1000);
			var min:Number = Math.floor(totalSec/60);
			var extraSec:Number = totalSec - (min*60);
			var totalTime:String = "" + min + ":" + FunLib.str_pad(extraSec, 2);
			
			this.timePlayed.text = totalTime;
			this.fruitsSliced.text = "" + fruits;
			this.totalScore.text = "" + score;
			
			
			addEventListener(Event.ADDED_TO_STAGE, addedToStage);
		}
		
		protected function addedToStage(e:Event):void
		{
			TweenMax.to(this, .5, {y: 5, ease: Cubic.easeOut});
			
			
			removeEventListener(Event.ADDED_TO_STAGE, addedToStage);
		}
	}
}