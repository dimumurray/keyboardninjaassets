package hud
{
	import com.greensock.TweenMax;
	import com.greensock.easing.Cubic;
	
	import flash.display.MovieClip;
	import flash.display.SimpleButton;
	import flash.display.StageQuality;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	
	public class GameHUD extends MovieClip
	{
		protected var _score:Number = 0;
		public var score:TextField;
		
		protected var _livesLeft:Number = 3;
		
		public var x1:MovieClip;
		public var x2:MovieClip;
		public var x3:MovieClip;
		
		public var monkeyFace:MovieClip;
		
		public var muteMusic:SimpleButton;
		public var muteSFX:SimpleButton;
		
		protected var _gameOver:GameOver;
		protected var _startTime:Date = new Date();
		
		protected var _fruitsSliced:Number = 0;
		
		protected var _gamePaused:Boolean = false;
		protected var _gameEnded:Boolean = false;
		
		
		public function GameHUD()
		{
			
		}
		
		
		public function increaseScore(amount:Number):void
		{
			_fruitsSliced++;
			_score += amount;
			this.score.text = "" + _score;
		}
		
		public function fruitMissed():void
		{
			--_livesLeft;
				
			switch(_livesLeft) {
				case 2: 
					// Make first X red
					x1.gotoAndStop(2);
					break;
				case 1:
					// Make second X red
					x2.gotoAndStop(2);
					break;
				case 0:
					// Make third X red
					x3.gotoAndStop(2);
					dispatchEvent(new Event(EngineEvent.GAME_OVER));
					trace("YOU LOSE!");
					break;
			}
		}
		
		public function pokeMonkey():void
		{
			monkeyFace.gotoAndPlay(2);
		}
		
		public function feedBanana():void
		{
			monkeyFace.gotoAndPlay(16);
		}
		
		public function showHUD():void
		{
			TweenMax.to(this, .5, {y: 0, ease:Cubic.easeOut});
		}
		
		public function hideHUD():void
		{
			TweenMax.to(this, .5, {y: -400, ease:Cubic.easeIn});
		}
		
		public function gameOver():void
		{
			try {
				stage.quality = StageQuality.HIGH;
			} catch(e:Error) {
				trace("Error changing quality");
			}
			
			TweenMax.to(this, .5, {y: -400, ease:Cubic.easeIn});
			
			_gameOver = new GameOver(_startTime, _fruitsSliced, _score);
			parent.addChild(_gameOver);
		}
		
		protected function resetHUD():void
		{
			
		}
		
		public function removeGameOver():void
		{
			if(_gameOver.parent) {
				_gameOver.parent.removeChild(_gameOver);
			}
		}
		
		
		
	}
}