package fruits
{
	import com.funtotype.media.FunSound;
	
	import graphics.Splatter;
	
	
	public class Banana extends Fruit
	{
		public function Banana(letter:String)
		{
			super(letter);
		}
		
		override public function letterTyped():void
		{
			super.letterTyped();
			FunSound.playSound("fruitSlice");
			
//			parent.addChildAt(new Splatter(x, y, Splatter.LEMON), 1);
		}
	}
}