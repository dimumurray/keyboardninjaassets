package fruits
{
	import com.funtotype.media.FunSound;
	import com.funtotype.utils.FunLib;
	
	import flash.filters.DropShadowFilter;
	
	import graphics.DoublePoints;
	import graphics.Splatter;
	
	
	public class Pineapple extends Fruit implements IPowerup
	{
		public function Pineapple(letter:String)
		{
			super(letter);
		}
		
		override protected function init():void
		{
			// fruit Animation
			fruit.stop();
			fruit.rotation = Math.random()*360;
			
			_gravity = .340;
			_speed = 12;
			
			if(Math.round(Math.random()) == 1) {
				x = _start.x = -100;
				y = _start.y = 420;
				
				_end.x = 1016;
			} else {
				x = _start.x = 1016;
				y = _start.y = 420;
				
				_end.x = -100;
			}
			
			_end.y = 200;
			
			// Velocity can be speed up or slowed down by modifying the 40/20
			_vx = (_end.x - _start.x);
			_vy = (_end.y - _start.y);
			
			_vx = dx * _speed;
			_vy = dy * (_speed * 1.25);
			
			
			// Drop shadow on fruit
			var dropShadow:DropShadowFilter = new DropShadowFilter(3, 90, 0, .5);
			
			fruit.filters = new Array(dropShadow);
			
			//			FunSound.playSound("cannonBlast");
		}
		
		override public function letterTyped():void
		{
			super.letterTyped();
			FunSound.playSound("fruitSlice");
			
			parent.addChild(new DoublePoints(x, y));
			
			parent.addChildAt(new Splatter(x, y, Splatter.LEMON), 1);
		}
	}
}