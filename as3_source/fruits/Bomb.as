package fruits
{
	import com.funtotype.media.FunSound;
	import com.funtotype.utils.FunLib;
	import com.greensock.plugins.GlowFilterPlugin;
	
	import flash.display.MovieClip;
	import flash.filters.GlowFilter;
	import flash.geom.Point;
	
	import graphics.Spark;
	
	public class Bomb extends Fruit
	{
		protected var _sparkCounter:Number = 0;
		
		public var bombSpark:MovieClip;
		
		public function Bomb(letter:String)
		{
			super(letter);
		}
		
		override public function letterTyped():void
		{
			super.letterTyped();
			FunSound.playSound("bombExplode");
			
			FunSound.playSound("swordSlice2", 1, 100);
		}
		
		override public function update():void
		{
			super.update();
			
			if(++_sparkCounter == 1) {
				
				// -31.1, -42.2
				var newLocal:Point = fruit.localToGlobal(new Point(FunLib.randBounds(-32, -30), FunLib.randBounds(-43, -41)));
				
				parent.addChild(new Spark(newLocal.x, newLocal.y));
				_sparkCounter = 0;
			}
		}
		
	}
}