package fruits
{
	import com.funtotype.media.FunSound;
	
	import graphics.Splatter;
	
	
	public class Coconut extends Fruit
	{
		public function Coconut(letter:String)
		{
			super(letter);
		}
		
		override public function letterTyped():void
		{
			super.letterTyped();
			FunSound.playSound("fruitSlice");
			
			parent.addChildAt(new Splatter(x, y, Splatter.COCONUT), 1);
		}
	}
}