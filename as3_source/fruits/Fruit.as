package fruits
{
	import com.funtotype.media.FunSound;
	import com.funtotype.utils.FunLib;
	
	import flash.display.MovieClip;
	import flash.filters.DropShadowFilter;
	import flash.geom.Point;
	import flash.text.TextField;

	public class Fruit extends MovieClip
	{
		/**
		 * Medium:
		 *  -Gravity: .370
		 *  -VX:      8
		 *  -VY:      16
		 */
		protected var _gravity:Number = .370;
		protected var _speed:Number = 8;
		protected var _rotation:Number = FunLib.randBounds(1, 6);
		
		protected var _start:Point = new Point();
		protected var _end:Point = new Point();
		
		protected var _vx:Number = 0;
		protected var _vy:Number = 0;
		
		protected var _dx:Number = 0;
		protected var _dy:Number = 0;
		
		protected var _xPos:Number = 0;
		protected var _yPos:Number = 0;
		
		protected var _letter:String;
		
		public var fruit:MovieClip;
		public var letterBox:TextField;
		
		protected var _fruitFrozen:Boolean = false;
		
		public function Fruit(letter:String)
		{
			init();
			
			// Random Letter
			_letter = letterBox.text = letter;
		}
		
		protected function init():void
		{
			// fruit Animation
			fruit.stop();
			fruit.rotation = Math.random()*360;
			
			x = _start.x = FunLib.randBounds(250, 666);
			y = _start.y = 420;
			
			_end.x = FunLib.randBounds(_start.x - 150, _start.x + 150);
			_end.y = FunLib.randBounds(25, 50);
			
			// Velocity can be speed up or slowed down by modifying the 40/20
			_vx = (_end.x - _start.x);
			_vy = (_end.y - _start.y);
			
			_vx = dx * _speed;
			_vy = dy * (_speed * 2);
			
			
			// Drop shadow on fruit
			var dropShadow:DropShadowFilter = new DropShadowFilter(3, 90, 0, .5);
			
			fruit.filters = new Array(dropShadow);
			
//			FunSound.playSound("cannonBlast");
		}
		
		public function update():void
		{
			_vy += _gravity;
			
			x += _vx;
			y += _vy;
			
			fruit.rotation += _rotation;
		}
		
		public function get letter():String
		{
			return _letter;
		}
		
		public function letterTyped():void
		{
			letterBox.visible = false;
			fruit.addFrameScript(24, destroy);
			fruit.play();
		}
		
		public function missedLetter():void
		{
			destroy();
		}
		
		protected function destroy():void
		{
			if(parent) {
				parent.removeChild(this);
			}
		}
		
		
		public function freezeFruit():void
		{
			if(!_fruitFrozen) {
				_fruitFrozen = true;
				_vx *= .5;
				_vy *= .5;
				_rotation *= .5;
				_gravity *= .25;
				_speed *= .5;
			}
		}
		
		public function defrostFruit():void
		{
			if(_fruitFrozen) {
				_fruitFrozen = false;
				_vx *= 2;
				_vy *= 2;
				_rotation *= 2;
				_gravity *= 4;
				_speed *= 2;
			}
		}
		
		
		
		public function get vx():Number
		{
			return _vx;
		}
		
		public function set vx(amount:Number):void
		{
			_vx = amount;
		}
		
		public function get vy():Number
		{
			return _vy;
		}
		
		public function set vy(amount:Number):void
		{
			_vy = amount;
		}
		
		public function get m():Number
		{
			return Math.sqrt(_vx * _vx + _vy * _vy);
		}
		
		public function get dx():Number
		{
			return _vx / m;
		}
		
		public function get dy():Number
		{
			return _vy / m;
		}
		
	}
}