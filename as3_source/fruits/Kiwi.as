package fruits
{
	import com.funtotype.media.FunSound;
	
	import graphics.Splatter;
	
	
	public class Kiwi extends Fruit
	{
		public function Kiwi(letter:String)
		{
			super(letter);
		}
		
		override public function letterTyped():void
		{
			super.letterTyped();
			FunSound.playSound("fruitSlice");
			
			parent.addChildAt(new Splatter(x, y, Splatter.KIWI), 1);
		}
	}
}