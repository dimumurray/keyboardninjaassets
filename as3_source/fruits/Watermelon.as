package fruits
{
	import com.funtotype.media.FunSound;
	
	import graphics.Splatter;
	

	public class Watermelon extends Fruit
	{
		public function Watermelon(letter:String)
		{
			super(letter);
//			fruit.scaleX = fruit.scaleY = .5;
		}
		
		override public function letterTyped():void
		{
			super.letterTyped();
			FunSound.playSound("fruitSlice");
			
			parent.addChildAt(new Splatter(x, y, Splatter.WATERMELON), 1);
		}
	}
}