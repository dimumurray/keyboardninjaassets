package fruits
{
	import com.funtotype.media.FunSound;
	
	import graphics.Splatter;
	
	
	public class RedApple extends Fruit
	{
		public function RedApple(letter:String)
		{
			super(letter);
		}
		
		override public function letterTyped():void
		{
			super.letterTyped();
			FunSound.playSound("fruitSlice");
			
			parent.addChildAt(new Splatter(x, y, Splatter.REDAPPLE), 1);
		}
	}
}