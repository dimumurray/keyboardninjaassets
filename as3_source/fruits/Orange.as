package fruits
{
	import com.funtotype.media.FunSound;
	
	import graphics.Splatter;
	
	
	public class Orange extends Fruit
	{
		public function Orange(letter:String)
		{
			super(letter);
		}
		
		override public function letterTyped():void
		{
			super.letterTyped();
			FunSound.playSound("fruitSlice");
			
			parent.addChildAt(new Splatter(x, y, Splatter.ORANGE), 1);
		}
	}
}