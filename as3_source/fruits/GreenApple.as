package fruits
{
	import com.funtotype.media.FunSound;
	
	import graphics.Splatter;
	
	
	public class GreenApple extends Fruit
	{
		public function GreenApple(letter:String)
		{
			super(letter);
		}
		
		override public function letterTyped():void
		{
			super.letterTyped();
			FunSound.playSound("fruitSlice");
			
			parent.addChildAt(new Splatter(x, y, Splatter.GREENAPPLE), 1);
		}
	}
}