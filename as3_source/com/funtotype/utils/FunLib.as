package com.funtotype.utils
{
   import flash.display.DisplayObject;
   import flash.display.DisplayObjectContainer;
   import flash.display.Shape;
   import flash.geom.Point;
   
   public class FunLib extends Object
   {
      
      public function FunLib()
      {
         super();
         if(_instance)
         {
            throw new Error("FunLib cannot be instantiated.");
         }
         else
         {
            return;
         }
      }
      
      private static var _instance:FunLib = new FunLib();
      
      public static function calcAngle(param1:Number, param2:Number, param3:Number, param4:Number) : Object
      {
         var _loc5_:Object = new Object();
         var _loc6_:Number = Math.atan2(param4 - param2,param3 - param1);
         _loc5_.dx = Math.cos(_loc6_);
         _loc5_.dy = Math.sin(_loc6_);
         _loc5_.rotation = _loc6_ * 180 / Math.PI;
         return _loc5_ as Object;
      }
      
      public static function getDistance(param1:Number, param2:Number, param3:Number, param4:Number) : Object
      {
         return Math.sqrt(Math.pow(param4 - param1,2) + Math.pow(param4 - param2,2));
      }
      
      public static function getRotation(param1:Number, param2:Number, param3:Number, param4:Number) : Number
      {
         var _loc5_:Number = Math.atan2(param4 - param2,param3 - param1);
         return _loc5_ * 180 / Math.PI;
      }
      
      public static function randBounds(param1:Number, param2:Number) : Number
      {
         return Math.floor(Math.random() * (param2 - param1 + 1)) + param1;
      }
      
      public static function trueRandBounds(param1:Number, param2:Number) : Number
      {
         return Math.random() * (param2 - param1 + 1) + param1;
      }
      
      public static function str_pad(param1:int, param2:int) : String
      {
         var _loc3_:String = "" + param1;
         while(_loc3_.length < param2)
         {
            _loc3_ = "0" + _loc3_;
         }
         return _loc3_;
      }
      
      public static function checkDomain(param1:DisplayObject) : Boolean
      {
         var _loc2_:int = param1.loaderInfo.loaderURL.indexOf("://") + 3;
         var _loc3_:int = param1.loaderInfo.loaderURL.indexOf("/",_loc2_);
         var _loc4_:String = param1.loaderInfo.loaderURL.substring(_loc2_,_loc3_);
         var _loc5_:int = _loc4_.lastIndexOf(".") - 1;
         var _loc6_:int = _loc4_.lastIndexOf(".",_loc5_) + 1;
         _loc4_ = _loc4_.substring(_loc6_,_loc4_.length);
         if(_loc4_ == "funtotype.com" || _loc4_ == "apexdevil.com" || _loc4_ == "funtotype.local" || _loc4_ == "edutyping.com" || _loc4_ == "edutyping.local" || _loc4_ == "typingweb.com" || _loc4_ == "typingace.com" || _loc4_ == "funtotype.loc" || _loc4_ == "edutyping.loc")
         {
            return true;
         }
         return false;
      }
      
      public static function trimString(param1:String) : String
      {
         if(param1 == null)
         {
            return "";
         }
         return param1.replace(new RegExp("^\\s+|\\s+$","g"),"");
      }
      
      public function traceAllChildren(param1:DisplayObjectContainer) : void
      {
         var item:* = undefined;
         var rootContainer:DisplayObjectContainer = param1;
         var i:Number = 0;
         while(i < rootContainer.numChildren)
         {
            item = rootContainer.getChildAt(i);
            try
            {
               this.traceAllChildren(item);
            }
            catch(e:Error)
            {
               trace(e.toString());
            }
            trace(item.toString());
            i++;
         }
      }
      
      public function createArrow(param1:Point, param2:Point, param3:Point, param4:uint = 65535) : Shape
      {
         var _loc5_:Number = Math.round(param2.x - param3.x);
         var _loc6_:Shape = new Shape();
         _loc6_.graphics.moveTo(param1.x,param1.y);
         _loc6_.graphics.beginFill(param4);
         if(param2.x - _loc5_ / 2 < param1.x)
         {
            if(param2.y < param1.y)
            {
               _loc6_.graphics.lineTo(param3.x,param3.y);
               _loc6_.graphics.lineTo(param2.x,param2.y);
               _loc6_.graphics.lineTo(param2.x,param2.y + 3);
               _loc6_.graphics.lineTo(param3.x - 5,param2.y + 3);
            }
            else
            {
               _loc6_.graphics.lineTo(param3.x,param3.y);
               _loc6_.graphics.lineTo(param2.x,param2.y);
               _loc6_.graphics.lineTo(param2.x,param2.y + 3);
               _loc6_.graphics.lineTo(param3.x + 5,param3.y + 3);
            }
         }
         else if(param2.y < param1.y)
         {
            _loc6_.graphics.lineTo(param2.x,param2.y);
            _loc6_.graphics.lineTo(param3.x,param3.y);
            _loc6_.graphics.lineTo(param3.x,param3.y + 3);
            _loc6_.graphics.lineTo(param2.x + 5,param2.y + 3);
         }
         else
         {
            _loc6_.graphics.lineTo(param2.x,param2.y);
            _loc6_.graphics.lineTo(param3.x,param3.y);
            _loc6_.graphics.lineTo(param3.x,param3.y + 3);
            _loc6_.graphics.lineTo(param2.x - 5,param2.y + 3);
         }
         
         _loc6_.graphics.endFill();
         return _loc6_;
      }
   }
}
