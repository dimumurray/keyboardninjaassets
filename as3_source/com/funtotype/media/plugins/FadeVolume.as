package com.funtotype.media.plugins
{
   import flash.utils.Timer;
   import flash.events.TimerEvent;
   import com.funtotype.media.FunSound;
   
   public class FadeVolume extends Object
   {
      
      public function FadeVolume(param1:String, param2:Number, param3:String)
      {
         var _loc4_:* = NaN;
         super();
         this._soundName = param1;
         this._startVolume = this._curVolume = FunSound.getVolume(param1) * 100;
         if(param3 == FadeVolume.FADE_OUT)
         {
            _loc4_ = Math.ceil(param2 / this._startVolume);
            this._volumeTimer = new Timer(_loc4_,this._startVolume);
            this._volumeTimer.addEventListener(TimerEvent.TIMER,this.fadeTick);
            this._volumeTimer.addEventListener(TimerEvent.TIMER_COMPLETE,this.destroy);
            this._volumeTimer.start();
         }
         else
         {
            _loc4_ = Math.ceil(param2 / 100);
            this._volumeTimer = new Timer(_loc4_,100);
            this._volumeTimer.addEventListener(TimerEvent.TIMER,this.fadeIn);
            this._volumeTimer.addEventListener(TimerEvent.TIMER_COMPLETE,this.endFadeIn);
            this._volumeTimer.start();
         }
      }
      
      public static const FADE_OUT:String = "fadeOut";
      
      public static const FADE_IN:String = "fadeIn";
      
      protected var _soundName:String;
      
      protected var _startVolume:Number;
      
      protected var _curVolume:Number;
      
      protected var _volumeTimer:Timer;
      
      protected function fadeTick(param1:TimerEvent) : void
      {
         if(--this._curVolume > 0)
         {
            FunSound.setVolume(this._soundName,this._curVolume / 100);
         }
      }
      
      protected function fadeIn(param1:TimerEvent) : void
      {
         if(++this._curVolume <= 100)
         {
            FunSound.setVolume(this._soundName,this._curVolume / 100);
         }
      }
      
      protected function endFadeIn(param1:TimerEvent) : void
      {
         this._volumeTimer.removeEventListener(TimerEvent.TIMER,this.fadeIn);
         this._volumeTimer.removeEventListener(TimerEvent.TIMER_COMPLETE,this.destroy);
         this._volumeTimer = null;
      }
      
      protected function destroy(param1:TimerEvent) : void
      {
         FunSound.stopSound(this._soundName);
         this._volumeTimer.removeEventListener(TimerEvent.TIMER,this.fadeTick);
         this._volumeTimer.removeEventListener(TimerEvent.TIMER_COMPLETE,this.destroy);
         this._volumeTimer = null;
      }
   }
}
