package com.funtotype.media
{
   import flash.display.MovieClip;
   import flash.utils.getDefinitionByName;
   import flash.media.SoundChannel;
   import flash.media.SoundTransform;
   import flash.media.Sound;
   import com.funtotype.media.plugins.FadeVolume;
   
   public class FunSound extends MovieClip
   {
      
      public function FunSound()
      {
         super();
         if(_instance)
         {
            throw new Error("FunLib cannot be instantiated.");
         }
         else
         {
            return;
         }
      }
      
      private static var _instance:FunSound = new FunSound();
      
      protected static var soundHolder:Object = new Object();
      
      protected static var _sfxMuted:Boolean = false;
      
      protected static var _musicMuted:Boolean = false;
      
      public static function addSound(param1:String, param2:String, param3:Boolean = true) : void
      {
         var _loc4_:Class = null;
         try
         {
            _loc4_ = getDefinitionByName(param2) as Class;
            soundHolder[param1] = new Object();
            soundHolder[param1].sound = new _loc4_();
            soundHolder[param1].channel = new SoundChannel();
            soundHolder[param1].transform = new SoundTransform(1);
            soundHolder[param1].channel.soundTransform = soundHolder[param1].transform;
            soundHolder[param1].playHead = 0;
            soundHolder[param1].paused = false;
            soundHolder[param1].sfx = param3;
         }
         catch(e:*)
         {
         }
      }
      
      public static function addDynamicSound(param1:String, param2:Sound, param3:Boolean = true) : void
      {
         try
         {
            soundHolder[param1] = new Object();
            soundHolder[param1].sound = param2;
            soundHolder[param1].channel = new SoundChannel();
            soundHolder[param1].transform = new SoundTransform(1);
            soundHolder[param1].channel.soundTransform = soundHolder[param1].transform;
            soundHolder[param1].playHead = 0;
            soundHolder[param1].paused = false;
            soundHolder[param1].sfx = param3;
         }
         catch(e:*)
         {
         }
      }
      
      public static function playSound(param1:String, param2:Number = 1, param3:Number = 0) : void
      {
         try
         {
            if(soundHolder[param1].sfx == true && _sfxMuted == false || soundHolder[param1].sfx == false && _musicMuted == false)
            {
               if(soundHolder[param1])
               {
                  if(param3 > 0)
                  {
                     soundHolder[param1].playHead = param3;
                  }
                  soundHolder[param1].channel = soundHolder[param1].sound.play(soundHolder[param1].playHead,param2);
                  soundHolder[param1].channel.soundTransform = soundHolder[param1].transform;
                  soundHolder[param1].paused = false;
               }
            }
         }
         catch(e:*)
         {
         }
      }
      
      public static function pauseSound(param1:String) : void
      {
         if(soundHolder[param1])
         {
            soundHolder[param1].playHead = soundHolder[param1].channel.position;
            soundHolder[param1].channel.stop();
            soundHolder[param1].paused = true;
         }
      }
      
      public static function stopSound(param1:String) : void
      {
         try
         {
            if(soundHolder[param1])
            {
               soundHolder[param1].channel.stop();
               soundHolder[param1].playHead = 0;
            }
         }
         catch(e:*)
         {
         }
      }
      
      public static function restartSound(param1:String, param2:Number = 1) : void
      {
         if(soundHolder[param1])
         {
            soundHolder[param1].channel.stop();
            soundHolder[param1].playhead = 0;
            playSound(param1,param2);
         }
      }
      
      public static function setVolume(param1:String, param2:Number) : void
      {
         try
         {
            if(soundHolder[param1])
            {
               soundHolder[param1].transform.volume = param2;
               soundHolder[param1].channel.soundTransform = soundHolder[param1].transform;
            }
         }
         catch(e:*)
         {
         }
      }
      
      public static function getVolume(param1:String) : Number
      {
         try
         {
            if(soundHolder[param1])
            {
               return soundHolder[param1].transform.volume;
            }
         }
         catch(e:*)
         {
         }
         return 0;
      }
      
      public static function setPan(param1:String, param2:Number) : void
      {
         if(soundHolder[param1])
         {
            soundHolder[param1].transform.pan = param2;
            soundHolder[param1].channel.soundTransform = soundHolder[param1].transform;
         }
      }
      
      public static function toggleMute(param1:String) : void
      {
         if(soundHolder[param1])
         {
            if(soundHolder[param1].transform.volume > 0)
            {
               setVolume(param1,0);
            }
            else
            {
               setVolume(param1,1);
            }
         }
      }
      
      public static function isPaused(param1:String) : Boolean
      {
         if(soundHolder[param1])
         {
            return soundHolder[param1].paused;
         }
         return false;
      }
      
      public static function fadeVolumeOut(param1:String, param2:Number) : void
      {
         if(soundHolder[param1])
         {
            new FadeVolume(param1,param2,FadeVolume.FADE_OUT);
         }
      }
      
      public static function fadeVolumeIn(param1:String, param2:Number) : void
      {
         if(soundHolder[param1])
         {
            new FadeVolume(param1,param2,FadeVolume.FADE_IN);
         }
      }
      
      public static function stopAll() : void
      {
         var _loc1_:String = null;
         for(_loc1_ in soundHolder)
         {
            stopSound(_loc1_);
         }
      }
      
      public static function pauseAll() : void
      {
         var _loc1_:String = null;
         for(_loc1_ in soundHolder)
         {
            pauseSound(_loc1_);
         }
      }
      
      public static function muteSounds() : void
      {
         if(_sfxMuted)
         {
            _sfxMuted = false;
         }
         else
         {
            _sfxMuted = true;
         }
      }
      
      public static function muteFx() : void
      {
         var _loc1_:String = null;
         try
         {
            _sfxMuted = true;
            for(_loc1_ in soundHolder)
            {
               if(soundHolder[_loc1_])
               {
                  if(soundHolder[_loc1_].sfx == true)
                  {
                     stopSound(_loc1_);
                  }
               }
            }
         }
         catch(e:*)
         {
         }
      }
      
      public static function unMuteFx() : void
      {
         try
         {
            _sfxMuted = false;
         }
         catch(e:*)
         {
         }
      }
      
      public static function muteMusic() : void
      {
         if(_musicMuted)
         {
            _musicMuted = false;
         }
         else
         {
            _musicMuted = true;
         }
      }
   }
}
