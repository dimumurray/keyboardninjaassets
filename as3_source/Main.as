package
{
	import com.adobe.crypto.MD5;
	import com.funtotype.media.FunSound;
	import com.funtotype.utils.*;
	import com.greensock.TweenMax;
	import com.greensock.easing.*;
	import com.greensock.plugins.*;
	
	import error.*;
	
	import flash.display.LoaderInfo;
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.display.SimpleButton;
	import flash.display.Sprite;
	import flash.display.StageQuality;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.external.ExternalInterface;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.system.Security;
	import flash.system.System;
	import flash.text.TextField;
	import flash.utils.Timer;
	
	import fruits.*;
	
	import menus.HowToPlay;
	import menus.MainMenu;
	
	/**
	 * TODO:
	 * Create sprite container for all fruits
	 * Make bombs spawn at very back of bomb container
	 */
	
	public class Main extends MovieClip
	{
		// ********************************************************************
		// 
		// Vars
		// 
		// ********************************************************************
		
		protected var _fadeSquare:Shape = new Shape();
		protected var _mainMenu:MainMenu;
		
		protected var _gameEngine:Game;
		
		public static var musicMuted:Boolean = false;
		
		public static var hasPlayed:Boolean = false;
		
		// ******************************************************************************
		// XML
		// ******************************************************************************
		protected var _XMLLoader:URLLoader;
		protected var _difficulty:String;
		protected var _letterlist:String;
		protected var _loadedList:XML;
		
		public var passedStringValid:Boolean = false;
		
		
		public function Main()
		{
			Security.allowDomain("*");
			Security.allowInsecureDomain("*");
			ExternalInterface.marshallExceptions = true;
			
			var passedString:String = String(this.loaderInfo.parameters.stringData);
			passedStringValid = verifyPassedString(passedString);
			
			addEventListener(Event.ADDED_TO_STAGE, addedToStage);
		}
		
		public function verifyPassedString(passedString:String):Boolean
		{
			// Break the passed string up
			var secondsPassed:Number = Number(passedString.substr(0, passedString.length - 32));
			var hash:String = passedString.substr(passedString.length - 32);
			
			trace("Hash: " + secondsPassed + " | " + hash);
			
			// Verify the call was recent
			var date:Date = new Date();
			var seconds:Number = Math.round(date.time / 1000);
			
			if(seconds - secondsPassed > 300) {
				trace(seconds - secondsPassed);
				return false;
			}
			
			
			// Verify the hash is valid
			var testHash = MD5.hash(secondsPassed + getS());
			
			if(testHash != hash) {
				trace(testHash + " | " + hash);
				return false;
			}
			
			trace("All good here!");
			return true;
		}
		
		public function getS()
		{
			return "@309asdjlfj)(JASLKJA2(023941!z";
		}
		
		protected function addedToStage(e:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, addedToStage);
			// FunLib.checkDomain(root) && 
			if(passedStringValid) {
				
				FunSound.addSound("fruitSlice", "sounds.Melon");
				FunSound.addSound("swordSlice1", "sounds.SwordSlice1");
				FunSound.addSound("swordSlice2", "sounds.SwordSlice2");
				FunSound.addSound("fuse", "sounds.Fuse");
				FunSound.addSound("bombExplode", "sounds.BombExplode");
				FunSound.addSound("cannonBlast", "sounds.CannonBlast");
				FunSound.addSound("gong", "sounds.Gong");
				FunSound.addSound("fruitMissed", "sounds.FruitMissed");
				FunSound.addSound("freezing", "sounds.Freezing");
				FunSound.addSound("swordSlash", "sounds.SwordSlash");
				
				FunSound.addSound("missLetter1", "sounds.MissLetter1");
				FunSound.addSound("missLetter2", "sounds.MissLetter2");
				FunSound.addSound("missLetter3", "sounds.MissLetter3");
				FunSound.addSound("missLetter4", "sounds.MissLetter4");
				
				FunSound.addSound("music", "music.ThemeSong", false);
				
				FunSound.addSound("introSong", "music.IntroSong", false);
				
				init();
				
			} else {
				var wrongDomain:MovieClip = new WrongDomain();
				wrongDomain.x = 456;
				wrongDomain.y = 200;
				addChild(wrongDomain);
			}
		}
		
		protected function init():void
		{
			trace("init");
			if(!Main.musicMuted) {
				FunSound.setVolume("introSong", 1);
				FunSound.playSound("introSong", 1);
			}
			stage.quality = StageQuality.HIGH;
			_mainMenu = new MainMenu();
			addChild(_mainMenu);
			_mainMenu.addEventListener(EngineEvent.GAME_START, gameStart);
		}
			
		protected function gameStart(e:EngineEvent):void
		{
			_mainMenu.removeEventListener(EngineEvent.GAME_START, gameStart);
			
			_difficulty = e.data.difficulty;
			_letterlist = e.data.letterlist;
			
			fadeIn();
		}
		
		protected function fadeIn():void
		{
			_fadeSquare.graphics.beginFill(0x000000);
			_fadeSquare.graphics.drawRect(-10, -10, 936, 420);
			_fadeSquare.graphics.endFill();
			_fadeSquare.alpha = 0;
			
			addChild(_fadeSquare);
			
			TweenMax.to(_fadeSquare, 1, {alpha: 1, onComplete: loadXML});
		}
		
		protected function loadXML():void
		{
			var lastSlash:int = root.loaderInfo.loaderURL.lastIndexOf("/")+1;
			var newDomain:String = root.loaderInfo.loaderURL.substring(0, lastSlash);
			
			_XMLLoader = new URLLoader();
			_XMLLoader.load(new URLRequest(newDomain + "xml/" + _letterlist + ".xml"));
			
			_XMLLoader.addEventListener(Event.COMPLETE, fadeOut, false, 0, true);
		}
		
		protected function fadeOut(e:Event):void
		{
			_XMLLoader.removeEventListener(Event.COMPLETE, fadeOut); 
			_XMLLoader = null;
			
			FunSound.fadeVolumeOut("introSong", 1000);
			
			_loadedList = new XML(e.target.data);
			
			removeChild(_mainMenu);
			TweenMax.to(_fadeSquare, 1, {alpha: 0, onComplete: initGame, delay: .15});
		}
		
		protected function initGame():void
		{
			removeChild(_fadeSquare);
			
			if(!Main.musicMuted) {
				FunSound.playSound("music", int.MAX_VALUE);
			}
			
			_gameEngine = new Game(_difficulty, _loadedList);
			_gameEngine.addEventListener(EngineEvent.RETURN_TO_MENU, resetButton);
			
			stage.addChild(_gameEngine);
		}
		
		protected function resetButton(e:Event):void
		{
			_gameEngine.removeEventListener(EngineEvent.RETURN_TO_MENU, resetButton);
			
			_fadeSquare = new Shape();
			_mainMenu = null;
			
			stage.removeChild(_gameEngine);
			_gameEngine = null;
			
			
			
			_XMLLoader = null;
			_difficulty = null;
			_letterlist = null;
			_loadedList = null;
			
			init();
		}
		
		
		
	}
}