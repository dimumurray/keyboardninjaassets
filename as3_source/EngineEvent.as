package
{
	import flash.events.Event;
	
	public class EngineEvent extends Event
	{
		/* Events */
		public static const GAME_START:String = "gameStart";
		public static const RETURN_TO_MENU:String = "returnToMenu";
		public static const GAME_OVER:String = "gameOver";
		
		
		/* Data */
		protected var _data:Object;
		
		public function EngineEvent(type:String, data:Object = null, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			
			_data = data;
		}
		
		public function get data():Object
		{
			return _data;
		}
		
		override public function clone():Event
		{
			return new EngineEvent(type, data, bubbles, cancelable);
		}
	}
}