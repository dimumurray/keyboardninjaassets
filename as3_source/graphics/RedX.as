package graphics
{
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.utils.Timer;

	public class RedX extends MissedFruit
	{
		protected var _removeTimer:Timer;
		
		public function RedX()
		{
			gotoAndStop(2);
			
			addEventListener(Event.ADDED_TO_STAGE, addedToStage);
		}
		
		protected function addedToStage(e:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, addedToStage);
			
			_removeTimer = new Timer(1000, 1);
			_removeTimer.addEventListener(TimerEvent.TIMER_COMPLETE, removeSelf);
			_removeTimer.start();
		}
		
		
		protected function removeSelf(e:TimerEvent):void
		{
			_removeTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, removeSelf);
			_removeTimer = null;
			
			if(parent) {
				parent.removeChild(this);
			}
		}
	}
}