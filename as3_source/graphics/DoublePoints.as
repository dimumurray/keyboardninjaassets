package graphics
{
	import com.funtotype.media.FunSound;
	import com.greensock.TweenMax;
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.text.TextField;
	
	public class DoublePoints extends MovieClip
	{
		public function DoublePoints(xPos:Number, yPos:Number):void
		{
			x = xPos;
			y = yPos;
			
			scaleX = scaleY = .01;
			
			addEventListener(Event.ADDED_TO_STAGE, addedToStage);
		}
		
		protected function addedToStage(e:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, addedToStage);
			
			FunSound.playSound("gong", 1, 50);
			TweenMax.to(this, .2, {scaleX: 1, scaleY: 1});
			TweenMax.to(this, .2, {scaleX: .01, scaleY: .01, onComplete: removeSelf, delay: 1.2});
		}
		
		protected function removeSelf():void
		{
			if(parent) {
				parent.removeChild(this);
			}
		}
	}
}