package graphics
{
	import com.greensock.TweenMax;
	
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.events.Event;
	
	public class FlashFade extends MovieClip
	{
		protected var _fadeSquare:Shape = new Shape();
		
		public function FlashFade(fadeInTime:Number = 500, fadeOutTime:Number = 500, color:uint = 0xFFFFFF, fadeHoldTime:Number = 0, fadeDelay:Number = 0)
		{
			x = 0;
			y = 0;
			
			_fadeSquare.graphics.beginFill(color);
			_fadeSquare.graphics.drawRect(-10, -10, 936, 420);
			_fadeSquare.graphics.endFill();
			_fadeSquare.alpha = 0;
			
			addChild(_fadeSquare);
			
			TweenMax.to(_fadeSquare, fadeInTime / 1000, {alpha: 1, delay: fadeDelay / 1000});
			TweenMax.to(_fadeSquare, fadeOutTime / 1000, {alpha: 0, delay: fadeDelay / 1000 + fadeHoldTime / 1000, onComplete: removeSelf});
		}
		
		protected function removeSelf():void
		{
			if(parent) {
				parent.removeChild(this);
			}
		}
	}
}