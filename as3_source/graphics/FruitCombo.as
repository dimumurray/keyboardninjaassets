package graphics
{
	import com.funtotype.media.FunSound;
	import com.greensock.TweenMax;
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.text.TextField;
	
	public class FruitCombo extends MovieClip
	{
		public var comboNum:TextField;
		
		public function FruitCombo(xPos:Number, yPos:Number, amount:Number):void
		{
			x = xPos;
			y = yPos;
			comboNum.text = "" + amount;
			
			scaleX = scaleY = .01;
			
			addEventListener(Event.ADDED_TO_STAGE, addedToStage);
		}
		
		protected function addedToStage(e:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, addedToStage);
			
			FunSound.playSound("gong", 1, 50);
			TweenMax.to(this, .2, {scaleX: 1, scaleY: 1});
			TweenMax.to(this, .2, {scaleX: .01, scaleY: .01, onComplete: removeSelf, delay: 1.2});
		}
		
		protected function removeSelf():void
		{
			if(parent) {
				parent.removeChild(this);
			}
		}
	}
}