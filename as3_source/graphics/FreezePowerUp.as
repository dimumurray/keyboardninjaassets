package graphics
{
	import com.greensock.TweenMax;
	import com.greensock.easing.Linear;
	import com.greensock.easing.Sine;
	
	import flash.display.MovieClip;
	import flash.events.Event;
	
	public class FreezePowerUp extends MovieClip
	{
		public function FreezePowerUp()
		{
			x = 0;
			y = 0;
			alpha = 0;
			
			addEventListener(Event.ADDED_TO_STAGE, addedToStage);
		}
		
		protected function addedToStage(e:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, addedToStage);
			TweenMax.to(this, 1, {alpha: 1, ease:Sine.easeInOut});
		}
	}
}