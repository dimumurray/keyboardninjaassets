package graphics
{
	import com.funtotype.utils.FunLib;
	import com.greensock.TweenMax;
	
	import flash.display.MovieClip;
	import flash.events.Event;
	
	public class Splatter extends MovieClip
	{
//		public static var WATERMELON:uint = 1;
//		public static var LEMON:uint = 2;
//		public static var COCONUT:uint = 3;
		
		
		// Starting Frames
		public static const COCONUT:uint = 1;
		public static const GREENAPPLE:uint = 4;
		public static const KIWI:uint = 7;
		public static const LEMON:uint = 10;
		public static const ORANGE:uint = 13;
		public static const POMEGRANATE:uint = 16;
		public static const REDAPPLE:uint = 19;
		public static const WATERMELON:uint = 22;
		
		public function Splatter(locX:Number, locY:Number, fruit:uint = 1)
		{
			gotoAndStop(fruit + Math.round(Math.random()*2));
			
			x = locX;
			y = locY;
			
			rotation = Math.random()*360;
			
			addEventListener(Event.ADDED_TO_STAGE, addedToStage);
		}
		
		protected function addedToStage(e:Event):void
		{
			TweenMax.to(this, 1, {alpha: 0, onComplete: removeSelf, delay: 6});
		}
		
		protected function removeSelf():void
		{
			if(parent) {
				parent.removeChild(this);
			}
		}
		
	}
}