package graphics
{
	import flash.display.MovieClip;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	public class Spark extends MovieClip
	{
		protected var _timer:Timer;
		
		public function Spark(xPos:Number, yPos:Number)
		{
			x = xPos;
			y = yPos;
			
			rotation = Math.random()*360;
			_timer = new Timer(200, 1);
			_timer.addEventListener(TimerEvent.TIMER_COMPLETE, timerComplete);
			_timer.start();
		}
		
		protected function timerComplete(e:TimerEvent):void
		{
			_timer.removeEventListener(TimerEvent.TIMER_COMPLETE, timerComplete);
			if(parent) {
				parent.removeChild(this);
			}
		}
	}
}