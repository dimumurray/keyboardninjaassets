package menus
{
	import flash.display.MovieClip;
	import flash.display.SimpleButton;
	
	public class PauseMenu extends MovieClip
	{
		public var resume:SimpleButton;
		public var howTo:SimpleButton;
		public var quitGame:SimpleButton;
		
		public function PauseMenu()
		{
			x = 458;
			y = 200;
		}
	}
}