package menus
{
	import com.funtotype.media.FunSound;
	import com.greensock.TweenMax;
	import com.greensock.easing.Cubic;
	import com.greensock.plugins.*;
	
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.display.SimpleButton;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	import graphics.*;
	
	
	
	public class MainMenu extends MovieClip
	{
		public var playGame:SimpleButton;
		public var howToPlay:SimpleButton;
		
		public var letters:SimpleButton;
		public var homerow_letters:SimpleButton;
		public var toprow_letters:SimpleButton;
		public var bottomrow_letters:SimpleButton;
		public var numbers:SimpleButton;
		
		public var easy:SimpleButton;
		public var medium:SimpleButton;
		public var hard:SimpleButton;
		
		public var muteSFX:SimpleButton;
		public var muteMusic:SimpleButton;
		
		public const HOW_TO:Number = 1832;
		public const MAIN_SCREEN:Number = 916;
		public const CHOOSE_LETTERS:Number = 0;
		public const DIFFICULTY:Number = -916;
		
		
		protected var _phase:Number = MAIN_SCREEN;
		protected var _letterList:String;
		protected var _difficulty:String;
		
		protected var _howTo:HowToPlay;
		
		// Animations
		
		
		protected var _fadeSquare:Shape = new Shape();
		
		public function MainMenu()
		{
			addEventListener(Event.ADDED_TO_STAGE, addedToStage);
		}
		
		protected function addedToStage(e:Event):void
		{
			x = MAIN_SCREEN;
			y = 0;
			
			_howTo = new HowToPlay();
			addChild(_howTo);
			
			removeEventListener(Event.ADDED_TO_STAGE, addedToStage);
			addEventListener(MouseEvent.CLICK, clickHandler);
			
		}
		
		protected function clickHandler(e:MouseEvent):void
		{
			var buttonPos:Point;
			
			if(e.target.name == "muteSFX") {
				FunSound.muteSounds();
			} else if(e.target.name == "muteMusic") {
				FunSound.muteMusic();
				
				if(Main.musicMuted) {
					// Play music
					FunSound.playSound("introSong");
					Main.musicMuted = false;
				} else {
					// stop music
					FunSound.stopSound("introSong");
					Main.musicMuted = true;
				}
				
			}
			
			if(_phase == MAIN_SCREEN) {
				if(e.target.name == "playGame") {
					TweenMax.to(this, 2, {x: CHOOSE_LETTERS, ease: Cubic.easeInOut});
					_phase = CHOOSE_LETTERS;
					
					// Swap e.target with animation that removes itself
					buttonPos = new Point(e.target.x, e.target.y);
					
					var playGameButton:PlayGameButton = new PlayGameButton();
					
					playGameButton.x = buttonPos.x;
					playGameButton.y = buttonPos.y - 65;
					
					e.target.parent.removeChild(e.target);
					
					addChild(playGameButton);
					
					FunSound.playSound("swordSlash");
					
					
				} else if(e.target.name == "howToPlay") {

					TweenMax.to(this, 2, {x: HOW_TO, ease: Cubic.easeInOut});
					
					FunSound.playSound("swordSlash");
					
				} else if(e.target.name == "backButton") {
					TweenMax.to(this, 2, {x: MAIN_SCREEN, ease: Cubic.easeInOut});
				}
			} else if(_phase == CHOOSE_LETTERS) {
				if(e.target.name == "letters" || 
				   e.target.name == "homerow_letters" ||
				   e.target.name == "toprow_letters" || 
				   e.target.name == "bottomrow_letters" || 
				   e.target.name == "numbers") {
					_letterList = e.target.name;
					_phase = DIFFICULTY;
					
					FunSound.playSound("swordSlash");
					
					TweenMax.to(this, 2, {x: DIFFICULTY, ease: Cubic.easeInOut});
					
					buttonPos = new Point(e.target.x, e.target.y - 65);
					
					var letterButton:MovieClip;
					
					switch(e.target.name) {
						case "letters":
							letterButton = new AllLetters();
							letterButton.x = buttonPos.x;
							letterButton.y = buttonPos.y;
							
							e.target.parent.removeChild(e.target);
							addChild(letterButton);
							break;
						case "homerow_letters":
							letterButton = new HomeRow();
							letterButton.x = buttonPos.x;
							letterButton.y = buttonPos.y;
							
							e.target.parent.removeChild(e.target);
							addChild(letterButton);
							break;
						case "toprow_letters":
							letterButton = new TopRow();
							letterButton.x = buttonPos.x;
							letterButton.y = buttonPos.y;
							
							e.target.parent.removeChild(e.target);
							addChild(letterButton);
							break;
						case "bottomrow_letters":
							letterButton = new BottomRow();
							letterButton.x = buttonPos.x;
							letterButton.y = buttonPos.y;
							
							e.target.parent.removeChild(e.target);
							addChild(letterButton);
							break;
						case "numbers":
							letterButton = new NumberPad();
							letterButton.x = buttonPos.x;
							letterButton.y = buttonPos.y;
							
							e.target.parent.removeChild(e.target);
							addChild(letterButton);
							break;
					}
					
				}
			} else if(_phase == DIFFICULTY) {
				if(e.target.name == "easy" ||
				   e.target.name == "medium" ||
				   e.target.name == "hard") {
					
					FunSound.playSound("swordSlash");
					
					buttonPos = new Point(e.target.x, e.target.y - 65);
					
					var difficultyButton:MovieClip;
					
					switch(e.target.name) {
						case "easy":
							difficultyButton = new Easy();
							difficultyButton.x = buttonPos.x;
							difficultyButton.y = buttonPos.y;
							
							e.target.parent.removeChild(e.target);
							addChild(difficultyButton);
							break;
						case "medium":
							difficultyButton = new Medium();
							difficultyButton.x = buttonPos.x;
							difficultyButton.y = buttonPos.y;
							
							e.target.parent.removeChild(e.target);
							addChild(difficultyButton);
							break;
						case "hard":
							difficultyButton = new Hard();
							difficultyButton.x = buttonPos.x;
							difficultyButton.y = buttonPos.y;
							
							e.target.parent.removeChild(e.target);
							addChild(difficultyButton);
							break;
					}
					
					_phase = -999;
					_difficulty = e.target.name;
					
					removeEventListener(MouseEvent.CLICK, clickHandler);
					
					dispatchEvent(new EngineEvent(EngineEvent.GAME_START, {difficulty: _difficulty, letterlist: _letterList}));
				}
			}
		}
		
		
		
	}
}