package
{
	import com.funtotype.media.FunSound;
	import com.funtotype.utils.*;
	import com.greensock.TweenMax;
	import com.greensock.easing.*;
	import com.greensock.plugins.*;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.display.StageQuality;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.external.ExternalInterface;
	import flash.sampler.NewObjectSample;
	import flash.system.System;
	import flash.text.TextField;
	import flash.utils.Timer;
	
	import fruits.*;
	
	import graphics.ArrowPowerUp;
	import graphics.FlashFade;
	import graphics.FreezePowerUp;
	import graphics.FruitCombo;
	import graphics.RedX;
	
	import hud.GameHUD;
	
	/**
	 * TODO:
	 * Create sprite container for all fruits
	 * Make bombs spawn at very back of bomb container
	 */
	
	public class Game extends Sprite
	{
		// Data Storage
		protected var _fruits:Vector.<Fruit> = new Vector.<Fruit>;
		
		// Stage Declarations
		
		// Game Vars
		protected var _activeWave:Boolean = false;
		protected var _waveTimer:Timer;
		protected var _launchGroupTimer:Timer;
		
		protected var _currentWave:Number = 0;
		
		protected var _singleLaunchTimer:Timer;
		protected var _groupLaunchTimer:Timer;
		protected var _groupLaunchNum:Number = 0;
		protected var _fruitPerWave:Number;
		
		protected var _gameHUD:GameHUD;
		
		protected var _gameActive:Boolean = true;
		
		protected var _paused:Boolean = false;
		
		
		protected var _difficulty:String;
		protected var _letterList:XML;
		
		// Graphic Layers
		protected var _hudLayer:Sprite = new Sprite();
		protected var _splashLayer:Sprite = new Sprite();
		protected var _fruitLayer:Sprite = new Sprite();
		protected var _pauseLayer:Sprite = new Sprite();
		
		
		// Power Ups
		protected var _pointTimer:Timer;
		protected var _freezeTimer:Timer;
		
		protected var _doublePoints:Boolean = false;
		protected var _doublePointAnimation:ArrowPowerUp;
		
		protected var _freezeActive:Boolean = false;
		protected var _freezeGraphic:FreezePowerUp;
		
		// C-c-c-combo
		protected var _comboTime:Date;
		protected var _comboNumber:Number = 0;
		
		// ********************************************************************
		// 
		// Start of Game
		// 
		// ********************************************************************
		public function Game(difficulty:String, letterlist:XML)
		{
			_difficulty = difficulty;
			_letterList = letterlist;
			addEventListener(Event.ADDED_TO_STAGE, addedToStage);
		}
		
		
		
		
		
		// ********************************************************************
		// 
		// Constructions & Deconstruction
		// 
		// ********************************************************************
		protected function init():void
		{
			try {
				if(!Main.hasPlayed) {
					ExternalInterface.call("startGame");
					Main.hasPlayed = true;
				} else {
					ExternalInterface.call("restartGame");
				}
			} catch(e:Error) {
				
			}
			
			stage.quality = StageQuality.MEDIUM;
			stage.stageFocusRect = false;
			stage.focus = this;
			
			addChild(_splashLayer);
			addChild(_fruitLayer);
			addChild(_hudLayer);
			addChild(_pauseLayer);
			
			switch(_difficulty) {
				case "easy":
					_fruitPerWave = 1;
					break;
				case "medium":
					_fruitPerWave = 3;
					break;
				case "hard":
					_fruitPerWave = 4;
					break;
				default:
					throw new Error("No Difficulty found!");
			}
			
			// Create GameHUD
			_gameHUD = new GameHUD();
			_gameHUD.addEventListener(EngineEvent.GAME_OVER, gameOver);
			_gameHUD.y = -400;
			_hudLayer.addChild(_gameHUD);
			
			_gameHUD.showHUD();
			
			addEventListener(Event.ENTER_FRAME, enterFrame);
			stage.addEventListener(KeyboardEvent.KEY_DOWN, keyDown);
			stage.addEventListener(MouseEvent.CLICK, clickHandler);
		}
		
		protected function deconstructor():void
		{
			
		}
		
		
		
		
		// ********************************************************************
		// 
		// Event Handlers
		// 
		// ********************************************************************
		protected function addedToStage(e:Event):void
		{
			init();
			
			removeEventListener(Event.ADDED_TO_STAGE, addedToStage);
		}
		
		protected function clickHandler(e:MouseEvent):void
		{
			if(e.target.name == "muteSFX") {
				
				FunSound.muteSounds();
			} else if(e.target.name == "muteMusic") {
				
				FunSound.muteMusic();
				
				if(Main.musicMuted) {
					// Play music
					FunSound.playSound("music", int.MAX_VALUE);
					Main.musicMuted = false;
				} else {
					// stop music
					FunSound.stopSound("music");
					Main.musicMuted = true;
				}
			} else if(e.target.name == "mainMenu") {
				
				stage.removeEventListener(MouseEvent.CLICK, clickHandler);
				deconstructor();
				if(!Main.musicMuted) {
					FunSound.stopSound("music");
				}
				dispatchEvent(new Event(EngineEvent.RETURN_TO_MENU, true));
				
			} else if(e.target.name == "playAgain") {
				stage.removeEventListener(MouseEvent.CLICK, clickHandler);
				playAgain();
			}
		}
		
		protected function enterFrame(e:Event):void
		{
			if(!_paused) {
				if(!_activeWave && !_waveTimer) {
					// Create Timer
					if(!(++_currentWave % 10)) {
						_fruitPerWave++;
					}
					
					if(_gameActive) {
						_waveTimer = new Timer(FunLib.randBounds(1500, 2500), 1);
						_waveTimer.addEventListener(TimerEvent.TIMER, randomWave);
						_waveTimer.start();
					}
				}
				
				moveFruit();
				comboBreaker();
			}
			/*
			if(uint(Math.random()*50) == 25) {
			trace(System.totalMemory / 1024);
			}
			*/
		}
		
		protected function keyDown(e:KeyboardEvent):void
		{
			if(!_paused) {
				var letterTyped:String = String.fromCharCode(e.charCode);
				
				for(var i:Number = 0; i < _fruits.length; i++) {
					if(_fruits[i].letter == letterTyped) {
						
						if(_fruits[i] is Bomb) {
							
							// Check if there are other fruits on the stage that have the same letter
							
							for(var o:Number = 0; o < _fruits.length; o++) {
								if(_fruits[o].letter == letterTyped && !(_fruits[o] is Bomb)) {
									
									_fruits[o].letterTyped();
									_fruits.splice(_fruits.indexOf(_fruits[o]), 1);
									
									return;
								}
							}
							
							// if no other letters found, explode the bomb
							_fruits[i].letterTyped();
							_fruits.splice(_fruits.indexOf(_fruits[i]), 1);
							
							if(!bombOnStage) {
								FunSound.stopSound("fuse");
							}
							
							gameOver(new Event("gameOver"));
							stage.addChild(new FlashFade(500, 300, 0xFFFFFF, 1000, 500));
							
							return;
						}
						
						
						_fruits[i].letterTyped();
						
						_comboTime = new Date();
//						_comboStartTime = _comboTime.time;
						_comboNumber++;
						
						
						
						
						if(_fruits[i] is IPowerup) {
							if(_fruits[i] is Pineapple) {
								
								doublePoints();
								_fruits.splice(_fruits.indexOf(_fruits[i]), 1);
							} else if(_fruits[i] is Cherry) {
								
								sliceAllFruit();
							} else if(_fruits[i] is Ice) {
								
								// Ice function
								freezeFruit();
								_fruits.splice(_fruits.indexOf(_fruits[i]), 1);
							}
						} else {
							_fruits.splice(_fruits.indexOf(_fruits[i]), 1);
						}
						
						
						if(_fruits.length < 1) {
							endWave();
						}
						
						_gameHUD.increaseScore(_doublePoints ? 26 : 13);
						
						return;
					}
				}
				
				FunSound.playSound("missLetter" + Math.ceil(Math.random()*4));
				
				_gameHUD.increaseScore(-7);
				_gameHUD.pokeMonkey();
				_comboTime = null;
				_comboNumber = 0;
			}
		}
		
		protected function gameOver(e:Event):void
		{
			_gameHUD.removeEventListener(EngineEvent.GAME_OVER, gameOver);
			stage.removeEventListener(KeyboardEvent.KEY_DOWN, keyDown);
			_gameActive = false;
			
			ExternalInterface.call("gameOver");
			
			// Full screen flash
			
			
			// Clear stage of graphics
			//			removeChild(_fruitLayer);
			
			var gameOverTimer:Timer = new Timer(1000, 1);
			gameOverTimer.addEventListener(TimerEvent.TIMER, removeLayers, false, 0, true);
			gameOverTimer.start();
			FunSound.fadeVolumeOut("fuse", 300);
			
			if(_waveTimer) {
				_waveTimer.removeEventListener(TimerEvent.TIMER, randomWave);
				_waveTimer.stop();
				_waveTimer = null;
			}
			
			if(_pointTimer) {
				_pointTimer.stop();
				_pointTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, endDoublePoints);
				_hudLayer.removeChild(_doublePointAnimation);
				_pointTimer = null;
			}
			
			if(_freezeTimer) {
				_freezeTimer.stop();
				_freezeTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, endFreezeFruit);
				_hudLayer.removeChild(_freezeGraphic);
				_freezeTimer = null;
			}
			
			_comboTime = null;
			_comboNumber = 0;
			
			deconstructor();
		}
		
		protected function removeLayers(e:TimerEvent):void
		{
			removeChild(_fruitLayer);
			_gameHUD.gameOver();
			
			_activeWave = false;
			_waveTimer = null;
			_launchGroupTimer = null;
			
			_currentWave = 0;
			
			_singleLaunchTimer = null;
			_groupLaunchTimer = null;
			_groupLaunchNum = 0;
			_fruitPerWave = 0;
			
			_doublePoints = false;
			_freezeActive = false;
			
			_fruits = null;
			_fruits = new Vector.<Fruit>;
			
			removeEventListener(Event.ENTER_FRAME, enterFrame);
		}
		
		protected function playAgain():void
		{
			_gameHUD.removeGameOver();
			_hudLayer.removeChild(_gameHUD);
			_gameHUD = null;
			removeChild(_hudLayer);
			removeChild(_splashLayer);
			removeChild(_pauseLayer);
			
			_fruitLayer = new Sprite();
			_hudLayer = new Sprite();
			_splashLayer = new Sprite();
			_pauseLayer = new Sprite();
			
			_gameActive = true;
			init();
		}
		
		
		
		
		// ********************************************************************
		// 
		// Enter Frame Functions
		// 
		// ********************************************************************
		protected function moveFruit():void
		{
			if(_fruits.length > 0 && !_paused) {
				if(!_activeWave && _gameActive) {
					startWave();
				}
				
				var fruitsRemoved:Vector.<Fruit> = new Vector.<Fruit>;
				
				
				for(var i:Number = 0; i < _fruits.length; i++) {
					_fruits[i].update();
					
					if(_fruits[i].y >= 500 && _gameActive) {
						fruitsRemoved.push(_fruits[i]);
						if(!(_fruits[i] is Bomb || _fruits[i] is IPowerup)) {
							
							_gameHUD.fruitMissed();
							var redX:RedX = new RedX;
							
							redX.x = _fruits[i].x;
							redX.y = 375;
							redX.gotoAndPlay(2);
							
							FunSound.playSound("fruitMissed");
							
							_fruitLayer.addChild(redX);
						}
					}
				}
				
				if(fruitsRemoved.length) {
					removeFruit(fruitsRemoved);
					
					
					fruitsRemoved = null;
				}
			}
		}
		
		protected function removeFruit(removedFruits:Vector.<Fruit>):void
		{
			for(var o:Number = 0; o < removedFruits.length; o++) {
				_fruits.splice(_fruits.indexOf(removedFruits[o]), 1);
				removedFruits[o].missedLetter();
				if(removedFruits[o] is Bomb) {
					if(!bombOnStage) {
						FunSound.fadeVolumeOut("fuse", 300);
					}
				}
			}
			
			if(_fruits.length < 1) {
				endWave();
			}
		}
		
		protected function comboBreaker():void
		{
			if(_comboTime) {
				
				var currentTime:Date = new Date();
				
				if(currentTime.time - _comboTime.time > 300) {
				
					if(_comboNumber >= 3) {
						_hudLayer.addChild(new FruitCombo(FunLib.randBounds(300, 600), FunLib.randBounds(50, 100), _comboNumber));
						_gameHUD.increaseScore(13 * _comboNumber);
						_gameHUD.feedBanana();
					}
					
					_comboTime = null;
					_comboNumber = 0;
				}
			}
		}
		
		
		
		
		
		
		// ********************************************************************
		// 
		// Game Methods
		// 
		// ********************************************************************
		public function get bombOnStage():Boolean
		{
			for(var i:Number = 0; i < _fruits.length; i++) {
				if(_fruits[i] is Bomb) {
					return true;
				}
			}
			
			return false;
		}
		
		protected function doublePoints():void
		{
			if(!_doublePoints) {
				_doublePoints = true;
				
				_pointTimer = new Timer(10000, 1);
				_pointTimer.addEventListener(TimerEvent.TIMER_COMPLETE, endDoublePoints, false, 0, true);
				_pointTimer.start();
				
				if(!_doublePointAnimation) {
					_doublePointAnimation = new ArrowPowerUp();
				}
				_hudLayer.addChildAt(_doublePointAnimation, 0);
			}
		}
		
		protected function endDoublePoints(e:TimerEvent):void
		{
			_pointTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, endDoublePoints);
			_pointTimer = null;
			_doublePoints = false;
			_hudLayer.removeChild(_doublePointAnimation);
			_doublePointAnimation = null;
		}
		
		protected function sliceAllFruit():void
		{
			var fruitsRemoved:Vector.<Fruit> = new Vector.<Fruit>;
			
			for(var i:Number = 0; i < _fruits.length; i++) {
				
				if(!(_fruits[i] is Bomb)) {
					_fruits[i].letterTyped();
					
					_gameHUD.increaseScore(_doublePoints ? 26 : 13);
					fruitsRemoved.push(_fruits[i]);
				}
			}
			
			if(fruitsRemoved.length) {
				for(var o:Number = 0; o < fruitsRemoved.length; o++) {
					_fruits.splice(_fruits.indexOf(fruitsRemoved[o]), 1);
				}
			}
			
//			_fruits = new Vector.<Fruit>;
		}
		
		protected function freezeFruit():void
		{
			if(!_freezeActive) {
				FunSound.playSound("freezing");
				_freezeActive = true;
				
				if(!_freezeGraphic) {
					_freezeGraphic = new FreezePowerUp();
				}
				
				_freezeTimer = new Timer(10000, 1);
				_freezeTimer.addEventListener(TimerEvent.TIMER_COMPLETE, endFreezeFruit, false, 0, true);
				_freezeTimer.start();
				
				_hudLayer.addChildAt(_freezeGraphic, 0);
				
				slowAllFruit();
			}
		}
		
		protected function slowAllFruit():void
		{
			for(var i:Number = 0; i < _fruits.length; i++) {
				_fruits[i].freezeFruit();
			}
		}
		
		protected function speedAllFruit():void
		{
			for(var i:Number = 0; i < _fruits.length; i++) {
				_fruits[i].defrostFruit();
			}
		}
		
		protected function endFreezeFruit(e:TimerEvent):void
		{
			// CHECK FOR TIMER IN GAMEOVER
			_freezeTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, endFreezeFruit);
			_freezeTimer = null;
			_freezeActive = false;
			speedAllFruit();
			if(_freezeGraphic) {
				TweenMax.to(_freezeGraphic, .5, {alpha: 0, onComplete: removeFreezeFruit});
			} else {
				trace("Couldn't find freezeGraphic");
			}
			
		}
		
		protected function removeFreezeFruit():void
		{
			if(_freezeGraphic) {
				_hudLayer.removeChild(_freezeGraphic);
			}
			_freezeGraphic = null;
		}
		
		// ********************************************************************
		// Types of Launches
		// ********************************************************************
		protected function randomWave(e:TimerEvent):void
		{
			startWave();
			if(_waveTimer) {
				_waveTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, randomWave);
				_waveTimer = null;
			}
			
			
			// Give random precedence to all types of waves
			var randomNum:Number = Math.round(Math.random()*2);

			switch(randomNum) {
				case 0: // Launch one at a time
					launchTimer(_fruitPerWave, FunLib.randBounds(5, 300));
					break;
				case 1: // Launch groups of 2 on a delay
					if(_fruitPerWave > 1) {
						if(_fruitPerWave % 2) {
							launchFruit();
						}
						launchGroupTimer(2, FunLib.randBounds(250, 650), Math.floor(_fruitPerWave/2));
					} else {
						launchFruit();
					}
					break;
				case 2: // Launch all at once
					launchTimer(_fruitPerWave, 45);
					break;
			}
			
		}
		
		
		// Launch a single fruit -- all call this function
		protected function launchFruit():void
		{
			var bombChance:Number;
			var newFruit:*;
			var powerUpChance:Number = Math.round(Math.random() * 80);
			
			if(_difficulty == "easy") {
				bombChance = Math.round(Math.random() * (_fruitPerWave * 4));
			} else if(_difficulty == "medium") {
				bombChance = Math.round(Math.random() * (_fruitPerWave * 3));
			} else {
				bombChance = Math.round(Math.random() * (_fruitPerWave * 2));
			}
			
			
			
			
			if(bombChance == 1) {
				
				if(!bombOnStage) {
					FunSound.playSound("fuse", int.MAX_VALUE);
					FunSound.setVolume("fuse", 1);
				}
				
				newFruit = new Bomb(randomLetter);
			} else {
				
				var randFruit:Number = Math.round(Math.random()*8);
				
				switch(randFruit) {
					case 0:
						newFruit = new Watermelon(randomLetter);
						break;
					case 1:
						newFruit = new Lemon(randomLetter);
						break;
					case 2:
						newFruit = new Coconut(randomLetter);
						break;
					case 3:
						newFruit = new Banana(randomLetter);
						break;
					case 4:
						newFruit = new Kiwi(randomLetter);
						break;
					case 5:
						newFruit = new GreenApple(randomLetter);
						break;
					case 6:
						newFruit = new RedApple(randomLetter);
						break;
					case 7:
						newFruit = new Pomegranate(randomLetter);
						break;
					case 8:
						newFruit = new Orange(randomLetter);
						break;
				}
			}
			
			_fruitLayer.addChild(newFruit);
			_fruits.push(newFruit);
			
			if(powerUpChance == 1 || powerUpChance == 2) {
				
				newFruit = new Pineapple(randomLetter);
				_fruitLayer.addChild(newFruit);
				_fruits.push(newFruit);
			} else if(powerUpChance == 3) {
				
				newFruit = new Cherry(randomLetter);
				_fruitLayer.addChild(newFruit);
				_fruits.push(newFruit);
			} else if(powerUpChance == 4 || powerUpChance == 5) {
				
				newFruit = new Ice(randomLetter);
				_fruitLayer.addChild(newFruit);
				_fruits.push(newFruit);
			}
			
			if(_freezeActive) {
				newFruit.freezeFruit();
			}
				
		}
		
		// Launch a group of fruits
		protected function launchGroup(numFruit:Number):void
		{
			for(var i:uint = 0; i < numFruit; i++) {
				launchFruit();
			}
			FunSound.playSound("cannonBlast");
		}
		
		// Launch a set number of fruit on a delay
		protected function launchTimer(numFruits:Number, delay:Number):void
		{
			_singleLaunchTimer = new Timer(delay, numFruits);
			_singleLaunchTimer.addEventListener(TimerEvent.TIMER, launchTimerTick);
			_singleLaunchTimer.addEventListener(TimerEvent.TIMER_COMPLETE, launchTimerComplete);
			_singleLaunchTimer.start();
		}
		
		protected function launchTimerTick(e:TimerEvent):void
		{
			launchFruit();
			FunSound.playSound("cannonBlast");
		}
		
		protected function launchTimerComplete(e:TimerEvent):void
		{
			_singleLaunchTimer.removeEventListener(TimerEvent.TIMER, launchTimerTick);
			_singleLaunchTimer.removeEventListener(TimerEvent.TIMER, launchTimerComplete);
			_singleLaunchTimer = null;
		}
		
		// Launch groups of fruits on a delay
		protected function launchGroupTimer(fruitsInGroup:Number, delay:Number, numGroups:Number):void
		{
			_groupLaunchTimer = new Timer(delay, numGroups);
			_groupLaunchTimer.addEventListener(TimerEvent.TIMER, launchGroupTimerTick);
			_groupLaunchTimer.addEventListener(TimerEvent.TIMER_COMPLETE, launchGroupTimerComplete);
			_groupLaunchTimer.start();
			
			_groupLaunchNum = fruitsInGroup;
		}
		
		protected function launchGroupTimerTick(e:TimerEvent):void
		{
			launchGroup(_groupLaunchNum);
		}
		
		protected function launchGroupTimerComplete(e:TimerEvent):void
		{
			_groupLaunchTimer.removeEventListener(TimerEvent.TIMER, launchGroupTimerTick);
			_groupLaunchTimer.removeEventListener(TimerEvent.TIMER, launchGroupTimerComplete);
			_groupLaunchTimer = null;
			_groupLaunchNum = 0;
		}
		
		
		
		
		// ********************************************************************
		// 
		// Getters // Setters
		// 
		// ********************************************************************
		public function endWave():void
		{
			_activeWave = false;
			// This is where the timer should go
			
		}
		
		public function startWave():void
		{
			_activeWave = true;
		}
		
		protected function get randomLetter():String
		{
			return _letterList.letter[Math.round(FunLib.randBounds(0, _letterList.letter.length() - 1))];
		}
		
	}
}